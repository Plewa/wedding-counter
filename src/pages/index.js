import React from "react"
import styled, { createGlobalStyle } from "styled-components"

import Counter from "../components/Counter"

import backgroundImage from "../images/backgroundImage.jpeg"

const GlobalStyle = createGlobalStyle`
    html, body, #___gatsby {
        height: 100%;
    }
    
    body {
        margin: 0px;
    }
    
    div[role="group"][tabindex] {
        height: 100%;
    }
    
  *, *::before, *::after {
    box-sizing: border-box;
  }
`

const Container = styled.div`
  background-image: url(${backgroundImage});
  background-size: cover;
  height: 100%;
  
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

export default () =>
  <>
    <GlobalStyle/>
    <Container>
      <Counter/>
    </Container>
  </>
