import React from "react"
import styled from "styled-components"

const Title = styled.h1`
  margin: 0;
  padding-top: 80px;
  font-size: 28px;
  font-family: Dancing Script;
`

export default () => <Title>DO ŚLUBU POZOSTAŁO</Title>