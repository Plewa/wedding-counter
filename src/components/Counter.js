import React from "react"
import styled from "styled-components"

import Title from "./Title"
import Timer from "./Timer"

const Counter = styled.div`
  background-color: rgba(255,255,255,0.8);
  border-radius: 200px;
  
  display: flex;
  flex-direction: column;
  align-items: center;
  
  height: 400px;
  width: 400px;
  
  @media (max-width: 400px) {
    width: 350px;
    border-radius: 20px;
  }
`

export default () =>
  <Counter>
    <Title/>
    <Timer/>
  </Counter>