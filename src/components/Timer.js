import React, { useState, useEffect } from "react"
import styled from "styled-components"

const Timer = styled.h3`
  margin: 0;
  padding-top: 80px;
  font-size: 40px;
  font-family: Lato;
  
  @media (max-width: 400px) {
    font-size: 35px;
  }
`

const calcTime = () => {
  // Set the date we're counting down to
  const countDownDate = new Date("08/08/2020 16:00:00").getTime()

  // Get today's date and time
  const now = new Date().getTime()

  // Find the distance between now and the count down date
  const distance = countDownDate - now

  // Time calculations for days, hours, minutes and seconds
  const days = Math.floor(distance / (1000 * 60 * 60 * 24))
  const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))
  const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))
  const seconds = Math.floor((distance % (1000 * 60)) / 1000)

  return `${days}d ${hours}h ${minutes}m ${seconds}s`
}

export default () => {
  const [time, setTime] = useState(calcTime())

  useEffect(() => {
    const interval = setInterval(() => {
      setTime(calcTime())
    }, 1000)

    return () => {
      clearInterval(interval)
    }
  }, [])

  return <Timer>{time}</Timer>
}

